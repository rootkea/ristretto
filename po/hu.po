# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Balázs Meskó <mesko.balazs@fsf.hu>, 2018
# Gabor Kelemen <kelemeng at gnome dot hu>, 2009-2012
# Gábor P., 2019-2020
# gyeben <gyonkibendeguz@gmail.com>, 2016
# Imre Benedek <nucleo at indamail dot hu>, 2012
# SZERVÁC Attila <sas at 321 dot hu>, 2007-2009
msgid ""
msgstr ""
"Project-Id-Version: Xfce Apps\n"
"Report-Msgid-Bugs-To: xfce-i18n@xfce.org\n"
"POT-Creation-Date: 2019-09-11 00:30+0200\n"
"PO-Revision-Date: 2020-02-11 16:15+0000\n"
"Last-Translator: Gábor P.\n"
"Language-Team: Hungarian (http://www.transifex.com/xfce/xfce-apps/language/hu/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hu\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../src/main.c:62
msgid "Version information"
msgstr "Verzióinformációk"

#: ../src/main.c:66
msgid "Start in fullscreen mode"
msgstr "Indítás teljes képernyős módban"

#: ../src/main.c:70
msgid "Start a slideshow"
msgstr "Diavetítés indítása"

#: ../src/main.c:78
msgid "Show settings dialog"
msgstr "Beállítások ablak megjelenítése"

#: ../src/main.c:103
#, c-format
msgid ""
"%s: %s\n"
"\n"
"Try %s --help to see a full list of\n"
"available command line options.\n"
msgstr "%s: %s\n\nAz elérhető kapcsolók listájáért adja\nki a %s --help parancsot.\n"

#: ../src/main_window.c:53 ../ristretto.desktop.in.h:3
msgid "Image Viewer"
msgstr "Képmegjelenítő"

#: ../src/main_window.c:401
msgid "_File"
msgstr "_Fájl"

#. Icon-name
#: ../src/main_window.c:407
msgid "_Open..."
msgstr "_Megnyitás…"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:409
msgid "Open an image"
msgstr "Kép megnyitása"

#. Icon-name
#: ../src/main_window.c:413
msgid "_Save copy..."
msgstr "Más_olat mentése…"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:415
msgid "Save a copy of the image"
msgstr "A kép másolatának mentése"

#. Icon-name
#: ../src/main_window.c:419
msgid "_Properties..."
msgstr "T_ulajdonságok…"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:421
msgid "Show file properties"
msgstr "Fájltulajdonságok megjelenítése"

#. Icon-name
#: ../src/main_window.c:425 ../src/main_window.c:444
msgid "_Edit"
msgstr "S_zerkesztés"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:427
msgid "Edit this image"
msgstr "Kép szerkesztése"

#. Icon-name
#: ../src/main_window.c:431 ../src/preferences_dialog.c:258
#: ../src/properties_dialog.c:138
msgid "_Close"
msgstr "_Bezárás"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:433
msgid "Close this image"
msgstr "Kép bezárása"

#. Icon-name
#: ../src/main_window.c:437
msgid "_Quit"
msgstr "_Kilépés"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:439
msgid "Quit Ristretto"
msgstr "Kilépés a Ristrettoból"

#: ../src/main_window.c:450
msgid "_Open with"
msgstr "Megnyitás e_zzel"

#: ../src/main_window.c:456
msgid "_Sort by"
msgstr "Ren_dezés szempontja"

#. Icon-name
#: ../src/main_window.c:462
msgid "_Delete"
msgstr "_Törlés"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:464
msgid "Delete this image from disk"
msgstr "Kép törlése a lemezről"

#. Icon-name
#: ../src/main_window.c:468
msgid "_Clear private data..."
msgstr "Sze_mélyes adatok törlése…"

#. Icon-name
#: ../src/main_window.c:474
msgid "_Preferences..."
msgstr "B_eállítások…"

#: ../src/main_window.c:481
msgid "_View"
msgstr "_Nézet"

#. Icon-name
#: ../src/main_window.c:487
msgid "_Fullscreen"
msgstr "_Teljes képernyő"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:489
msgid "Switch to fullscreen"
msgstr "Átváltás teljes képernyőre"

#. Icon-name
#: ../src/main_window.c:493
msgid "_Leave Fullscreen"
msgstr "T_eljes képernyő elhagyása"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:495
msgid "Leave Fullscreen"
msgstr "Teljes képernyő elhagyása"

#. Icon-name
#: ../src/main_window.c:499
msgid "Set as _Wallpaper..."
msgstr "Beállítás _háttérképként…"

#: ../src/main_window.c:506
msgid "_Zoom"
msgstr "_Nagyítás"

#. Icon-name
#: ../src/main_window.c:512
msgid "Zoom _In"
msgstr "_Nagyítás"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:514
msgid "Zoom in"
msgstr "Nagyítás"

#. Icon-name
#: ../src/main_window.c:518
msgid "Zoom _Out"
msgstr "_Kicsinyítés"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:520
msgid "Zoom out"
msgstr "Kicsinyítés"

#. Icon-name
#: ../src/main_window.c:524
msgid "Zoom _Fit"
msgstr "_Igazítás az ablakhoz"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:526
msgid "Zoom to fit window"
msgstr "Illesztés az ablakhoz"

#. Icon-name
#: ../src/main_window.c:530
msgid "_Normal Size"
msgstr "_Normál méret"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:532
msgid "Zoom to 100%"
msgstr "100%-os nagyítás"

#: ../src/main_window.c:537
msgid "_Rotation"
msgstr "_Forgatás"

#. Icon-name
#: ../src/main_window.c:543
msgid "Rotate _Right"
msgstr "Forgatás _jobbra"

#. Icon-name
#: ../src/main_window.c:549
msgid "Rotate _Left"
msgstr "Forgatás _balra"

#: ../src/main_window.c:556
msgid "_Flip"
msgstr "_Forgatás"

#: ../src/main_window.c:562
msgid "Flip _Horizontally"
msgstr "Forgatás _vízszintesen"

#: ../src/main_window.c:568
msgid "Flip _Vertically"
msgstr "Forgatás _függőlegesen"

#: ../src/main_window.c:575
msgid "_Go"
msgstr "_Ugrás"

#. Icon-name
#: ../src/main_window.c:581
msgid "_Forward"
msgstr "_Előre"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:583
msgid "Next image"
msgstr "Következő kép"

#. Icon-name
#: ../src/main_window.c:587
msgid "_Back"
msgstr "_Vissza"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:589
msgid "Previous image"
msgstr "Előző kép"

#. Icon-name
#: ../src/main_window.c:593
msgid "F_irst"
msgstr "_Első"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:595
msgid "First image"
msgstr "Első kép"

#. Icon-name
#: ../src/main_window.c:599
msgid "_Last"
msgstr "_Utolsó"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:601
msgid "Last image"
msgstr "Utolsó kép"

#: ../src/main_window.c:606
msgid "_Help"
msgstr "_Súgó"

#. Icon-name
#: ../src/main_window.c:612
msgid "_Contents"
msgstr "_Tartalom"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:614
msgid "Display ristretto user manual"
msgstr "A Ristretto felhasználói kézikönyvének megjelenítése"

#. Icon-name
#: ../src/main_window.c:618
msgid "_About"
msgstr "_Névjegy"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:620
msgid "Display information about ristretto"
msgstr "Információk megjelenítése a Ristrettoról"

#: ../src/main_window.c:625
msgid "_Position"
msgstr "_Pozíció"

#: ../src/main_window.c:631
msgid "_Size"
msgstr "_Méret"

#: ../src/main_window.c:637
msgid "Thumbnail Bar _Position"
msgstr "Bélyegképsá_v helye"

#: ../src/main_window.c:643
msgid "Thumb_nail Size"
msgstr "Bélyegkép _mérete"

#. Icon-name
#: ../src/main_window.c:650
msgid "Leave _Fullscreen"
msgstr "T_eljes képernyő elhagyása"

#. Icon-name
#: ../src/main_window.c:668
msgid "_Show Toolbar"
msgstr "_Eszköztár megjelenítése"

#. Icon-name
#: ../src/main_window.c:676
msgid "Show _Thumbnail Bar"
msgstr "_Bélyegképsáv megjelenítése"

#. Icon-name
#: ../src/main_window.c:684
msgid "Show Status _Bar"
msgstr "Álla_potsor megjelenítése"

#. Icon-name
#: ../src/main_window.c:696
msgid "file name"
msgstr "Fájlnév"

#. Icon-name
#: ../src/main_window.c:702
msgid "file type"
msgstr "Fájltípus"

#. Icon-name
#: ../src/main_window.c:708
msgid "date"
msgstr "Dátum"

#. Icon-name
#: ../src/main_window.c:719
msgid "Left"
msgstr "Balra"

#: ../src/main_window.c:725
msgid "Right"
msgstr "Jobbra"

#: ../src/main_window.c:731
msgid "Top"
msgstr "Fent"

#: ../src/main_window.c:737
msgid "Bottom"
msgstr "Lent"

#: ../src/main_window.c:748
msgid "Very Small"
msgstr "Nagyon kicsi"

#: ../src/main_window.c:754
msgid "Smaller"
msgstr "Kisebb"

#: ../src/main_window.c:760
msgid "Small"
msgstr "Kicsi"

#: ../src/main_window.c:766
msgid "Normal"
msgstr "Normál"

#: ../src/main_window.c:772
msgid "Large"
msgstr "Nagy"

#: ../src/main_window.c:778
msgid "Larger"
msgstr "Nagyobb"

#: ../src/main_window.c:784
msgid "Very Large"
msgstr "Nagyon nagy"

#. Create Play/Pause Slideshow actions
#: ../src/main_window.c:941
msgid "_Play"
msgstr "_Lejátszás"

#: ../src/main_window.c:941
msgid "Play slideshow"
msgstr "Diavetítés lejátszása"

#: ../src/main_window.c:942
msgid "_Pause"
msgstr "S_zünet"

#: ../src/main_window.c:942
msgid "Pause slideshow"
msgstr "Diavetítés szüneteltetése"

#. Create Recently used items Action
#: ../src/main_window.c:945
msgid "_Recently used"
msgstr "_Nemrég használt"

#: ../src/main_window.c:945
msgid "Recently used"
msgstr "Nemrég használt"

#: ../src/main_window.c:1057 ../src/main_window.c:1673
msgid "Press open to select an image"
msgstr "Kép kiválasztásához nyomja meg a Megnyitás gombot"

#: ../src/main_window.c:1522 ../src/main_window.c:1526
msgid "Open With Other _Application..."
msgstr "Megnyitás _más alkalmazással…"

#: ../src/main_window.c:1547 ../src/main_window.c:1553
msgid "Empty"
msgstr "Üres"

#: ../src/main_window.c:1683
msgid "Loading..."
msgstr "Betöltés…"

#: ../src/main_window.c:2275
msgid "Choose 'set wallpaper' method"
msgstr "A „háttérkép-beállítási” mód kiválasztása"

#: ../src/main_window.c:2279 ../src/main_window.c:3275
#: ../src/main_window.c:3509 ../src/main_window.c:4153
#: ../src/privacy_dialog.c:182 ../src/xfce_wallpaper_manager.c:322
#: ../src/gnome_wallpaper_manager.c:238
msgid "_Cancel"
msgstr "_Mégse"

#: ../src/main_window.c:2282 ../src/main_window.c:4156
#: ../src/xfce_wallpaper_manager.c:328 ../src/gnome_wallpaper_manager.c:244
msgid "_OK"
msgstr "_OK"

#: ../src/main_window.c:2292 ../src/preferences_dialog.c:475
msgid ""
"Configure which system is currently managing your desktop.\n"
"This setting determines the method <i>Ristretto</i> will use\n"
"to configure the desktop wallpaper."
msgstr "Adja meg, hogy melyik rendszer kezeli jelenleg az asztalát.\nEz a beállítás meghatározza, hogy a <i>Ristretto</i> melyik\nmódszert használja az asztal háttérképének beállítására."

#: ../src/main_window.c:2318 ../src/preferences_dialog.c:487
msgid "None"
msgstr "Nincs"

#: ../src/main_window.c:2322 ../src/preferences_dialog.c:491
msgid "Xfce"
msgstr "Xfce"

#: ../src/main_window.c:2326 ../src/preferences_dialog.c:495
msgid "GNOME"
msgstr "GNOME"

#: ../src/main_window.c:2834
msgid "Developers:"
msgstr "Fejlesztők:"

#: ../src/main_window.c:2844 ../ristretto.appdata.xml.in.h:1
msgid "Ristretto is an image viewer for the Xfce desktop environment."
msgstr "A Ristretto egy képmegjelenítő az Xfce asztali környezethez."

#: ../src/main_window.c:2852
msgid "translator-credits"
msgstr "Kelemen Gábor <kelemeng at gnome dot hu>"

#: ../src/main_window.c:3272
msgid "Open image"
msgstr "Kép megnyitása"

#: ../src/main_window.c:3276
msgid "_Open"
msgstr "_Megnyitás"

#: ../src/main_window.c:3293
msgid "Images"
msgstr "Képek"

#: ../src/main_window.c:3298
msgid ".jp(e)g"
msgstr ".jp(e)g"

#: ../src/main_window.c:3325 ../src/main_window.c:3467
msgid "Could not open file"
msgstr "Nem lehet megnyitni a fájlt"

#: ../src/main_window.c:3506
msgid "Save copy"
msgstr "Másolat mentése"

#: ../src/main_window.c:3510
msgid "_Save"
msgstr "M_entés"

#: ../src/main_window.c:3539
msgid "Could not save file"
msgstr "Nem lehet menteni a fájlt"

#: ../src/main_window.c:3709
#, c-format
msgid "Are you sure you want to send image '%s' to trash?"
msgstr "Biztosan a Kukába dobja a(z) „%s” képet?"

#: ../src/main_window.c:3713
#, c-format
msgid "Are you sure you want to delete image '%s' from disk?"
msgstr "Biztosan törli a(z) „%s” képet a lemezről?"

#: ../src/main_window.c:3724
msgid "_Do not ask again for this session"
msgstr "_Ne kérdezze meg újra ebben a munkamenetben"

#: ../src/main_window.c:3801
#, c-format
msgid ""
"An error occurred when deleting image '%s' from disk.\n"
"\n"
"%s"
msgstr "Hiba történt a(z) „%s” kép törlésekor a lemezről.\n\n%s"

#: ../src/main_window.c:3805
#, c-format
msgid ""
"An error occurred when sending image '%s' to trash.\n"
"\n"
"%s"
msgstr "Hiba történt a(z) „%s” kép Kukába dobásakor\n\n%s"

#: ../src/main_window.c:4148
msgid "Edit with"
msgstr "Szerkesztés ezzel"

#: ../src/main_window.c:4169
#, c-format
msgid "Open %s and other files of type %s with:"
msgstr "Válasszon alkalmazást a(z) %s és más %s típusú fájlok megnyitásához:"

#: ../src/main_window.c:4175
msgid "Use as _default for this kind of file"
msgstr "_Legyen alapértelmezett ehhez a fájltípushoz"

#: ../src/main_window.c:4265
msgid "Recommended Applications"
msgstr "Javasolt alkalmazások"

#: ../src/main_window.c:4345
msgid "Other Applications"
msgstr "Más alkalmazások"

#: ../src/icon_bar.c:345
msgid "Orientation"
msgstr "Tájolás"

#: ../src/icon_bar.c:346
msgid "The orientation of the iconbar"
msgstr "Az ikonsáv tájolása"

#: ../src/icon_bar.c:362
msgid "File column"
msgstr "Fájl oszlop"

#: ../src/icon_bar.c:363
msgid "Model column used to retrieve the file from"
msgstr "A fájl letöltéséhez használt modelloszlop"

#: ../src/icon_bar.c:375
msgid "Icon Bar Model"
msgstr "Ikonsáv modellje"

#: ../src/icon_bar.c:376
msgid "Model for the icon bar"
msgstr "Az ikonsáv modellje"

#: ../src/icon_bar.c:392
msgid "Active"
msgstr "Aktív"

#: ../src/icon_bar.c:393
msgid "Active item index"
msgstr "Aktív elem indexe"

#: ../src/icon_bar.c:409 ../src/icon_bar.c:410
msgid "Show Text"
msgstr "Szöveg megjelenítése"

#: ../src/icon_bar.c:422
msgid "Scrolled window"
msgstr "Görgethető ablak"

#: ../src/icon_bar.c:423
msgid "Scrolled window icon bar is placed into"
msgstr ""

#: ../src/icon_bar.c:429 ../src/icon_bar.c:430
msgid "Active item fill color"
msgstr "Aktív elem kitöltőszíne"

#: ../src/icon_bar.c:436 ../src/icon_bar.c:437
msgid "Active item border color"
msgstr "Aktív elem szegélyszíne"

#: ../src/icon_bar.c:443 ../src/icon_bar.c:444
msgid "Active item text color"
msgstr "Aktív elem szövegszíne"

#: ../src/icon_bar.c:450 ../src/icon_bar.c:451
msgid "Cursor item fill color"
msgstr "Kurzorelem kitöltőszíne"

#: ../src/icon_bar.c:457 ../src/icon_bar.c:458
msgid "Cursor item border color"
msgstr "Kurzorelem szegélyszíne"

#: ../src/icon_bar.c:464 ../src/icon_bar.c:465
msgid "Cursor item text color"
msgstr "Kurzorelem szövegszíne"

#: ../src/privacy_dialog.c:152
msgid "Time range to clear:"
msgstr "Törlendő időszak:"

#: ../src/privacy_dialog.c:155
msgid "Cleanup"
msgstr "Törlés"

#: ../src/privacy_dialog.c:158
msgid "Last Hour"
msgstr "Utolsó óra"

#: ../src/privacy_dialog.c:159
msgid "Last Two Hours"
msgstr "Utolsó két óra"

#: ../src/privacy_dialog.c:160
msgid "Last Four Hours"
msgstr "Utolsó négy óra"

#: ../src/privacy_dialog.c:161
msgid "Today"
msgstr "Ma"

#: ../src/privacy_dialog.c:162
msgid "Everything"
msgstr "Minden"

#: ../src/privacy_dialog.c:185 ../src/xfce_wallpaper_manager.c:325
#: ../src/gnome_wallpaper_manager.c:241
msgid "_Apply"
msgstr "_Alkalmaz"

#: ../src/privacy_dialog.c:475
msgid "Clear private data"
msgstr "Személyes adatok törlése"

#: ../src/preferences_dialog.c:289
msgid "Display"
msgstr "Megjelenítés"

#: ../src/preferences_dialog.c:296
msgid "Background color"
msgstr "Háttérszín"

#: ../src/preferences_dialog.c:300
msgid "Override background color:"
msgstr "Háttérszín felülbírálása:"

#: ../src/preferences_dialog.c:327
msgid "Quality"
msgstr "Minőség"

#: ../src/preferences_dialog.c:331
msgid ""
"With this option enabled, the maximum image-quality will be limited to the "
"screen-size."
msgstr "A legjobb képminőség korlátozása a kijelző méretére"

#: ../src/preferences_dialog.c:335
msgid "Limit rendering quality"
msgstr "Megjelenítési minőség korlátozása"

#: ../src/preferences_dialog.c:347
msgid "Fullscreen"
msgstr "Teljes képernyő"

#: ../src/preferences_dialog.c:351
msgid "Thumbnails"
msgstr "Bélyegképek"

#: ../src/preferences_dialog.c:354
msgid ""
"The thumbnail bar can be automatically hidden when the window is fullscreen."
msgstr "A bélyegképsáv automatikusan elrejthető, ha az ablak teljes képernyőn van."

#: ../src/preferences_dialog.c:358
msgid "Hide thumbnail bar when fullscreen"
msgstr "Bélyegképsáv elrejtése teljes képernyőn"

#: ../src/preferences_dialog.c:366
msgid "Clock"
msgstr "Óra"

#: ../src/preferences_dialog.c:369
msgid ""
"Show an analog clock that displays the current time when the window is "
"fullscreen"
msgstr "Analóg óra megjelenítése, ha az ablak teljes képernyőn van"

#: ../src/preferences_dialog.c:373
msgid "Show Fullscreen Clock"
msgstr "Teljes képernyős óra megjelenítése"

#: ../src/preferences_dialog.c:379
msgid "Mouse cursor"
msgstr "Egér kurzor"

#: ../src/preferences_dialog.c:382
msgid ""
"The mouse cursor can be automatically hidden after a certain period of inactivity\n"
"when the window is fullscreen."
msgstr "Az egér kurzor automatikusan elrejthető egy idő után, ha az ablak teljes képernyőn van."

#: ../src/preferences_dialog.c:389
msgid "Period of inactivity (seconds):"
msgstr "Az inaktivitás időtartama (másodperc):"

#: ../src/preferences_dialog.c:408
msgid "Slideshow"
msgstr "Diavetítés"

#: ../src/preferences_dialog.c:412
msgid "Timeout"
msgstr "Időtúllépés"

#: ../src/preferences_dialog.c:415
msgid ""
"The time period an individual image is displayed during a slideshow\n"
"(in seconds)"
msgstr "Egyes képek megjelenítése ennyi ideig diavetítés során (másodperc)"

#: ../src/preferences_dialog.c:429
msgid "Control"
msgstr "Vezérlés"

#: ../src/preferences_dialog.c:433
msgid "Scroll wheel"
msgstr "Egérgörgő"

#: ../src/preferences_dialog.c:436
msgid "Invert zoom direction"
msgstr "Nagyítási irány megfordítása"

#: ../src/preferences_dialog.c:447
msgid "Behaviour"
msgstr "Viselkedés"

#: ../src/preferences_dialog.c:451
msgid "Startup"
msgstr "Indítás"

#: ../src/preferences_dialog.c:453
msgid "Maximize window on startup when opening an image"
msgstr "Ablak maximalizálása indításkor kép megnyitásakor"

#: ../src/preferences_dialog.c:459
msgid "Wrap around images"
msgstr "Képek körbefuttatása"

#: ../src/preferences_dialog.c:470
msgid "Desktop"
msgstr "Asztal"

#: ../src/preferences_dialog.c:580
msgid "Image Viewer Preferences"
msgstr "Képmegjelenítő beállításai"

#: ../src/properties_dialog.c:173
msgid "<b>Name:</b>"
msgstr "<b>Név:</b>"

#: ../src/properties_dialog.c:174
msgid "<b>Kind:</b>"
msgstr "<b>Típus:</b>"

#: ../src/properties_dialog.c:175
msgid "<b>Modified:</b>"
msgstr "<b>Módosítva:</b>"

#: ../src/properties_dialog.c:176
msgid "<b>Accessed:</b>"
msgstr "<b>Elérés:</b>"

#: ../src/properties_dialog.c:177
msgid "<b>Size:</b>"
msgstr "<b>Méret:</b>"

#: ../src/properties_dialog.c:201
msgid "General"
msgstr "Általános"

#: ../src/properties_dialog.c:207
msgid "Image"
msgstr "Kép"

#: ../src/properties_dialog.c:442
#, c-format
msgid "<b>Date taken:</b>"
msgstr "<b>Felvétel dátuma:</b>"

#: ../src/properties_dialog.c:454 ../src/properties_dialog.c:466
#: ../src/properties_dialog.c:478
#, c-format
msgid "<b>%s</b>"
msgstr "<b>%s</b>"

#: ../src/properties_dialog.c:538
#, c-format
msgid "%s - Properties"
msgstr "%s tulajdonságai"

#: ../src/thumbnailer.c:438
msgid ""
"The thumbnailer-service can not be reached,\n"
"for this reason, the thumbnails can not be\n"
"created.\n"
"\n"
"Install <b>Tumbler</b> or another <i>thumbnailing daemon</i>\n"
"to resolve this issue."
msgstr "A bélyegkép-készítő szolgáltatás nem érhető el, emiatt a bélyegképek nem hozhatók létre.\n\nTelepítse a <b>Tumblert</b> vagy egy másik <i>bélyegképkészítő démont</i> a probléma megoldásához."

#: ../src/thumbnailer.c:448
msgid "Do _not show this message again"
msgstr "_Ne jelenjen meg ez újra"

#: ../src/xfce_wallpaper_manager.c:302 ../src/gnome_wallpaper_manager.c:224
msgid "Style:"
msgstr "Stílus:"

#: ../src/xfce_wallpaper_manager.c:313
msgid "Apply to all workspaces"
msgstr "Alkalmazás az össze munkaterület esetében"

#: ../src/xfce_wallpaper_manager.c:320 ../src/gnome_wallpaper_manager.c:236
msgid "Set as wallpaper"
msgstr "Beállítás háttérképként"

#: ../src/xfce_wallpaper_manager.c:382 ../src/gnome_wallpaper_manager.c:294
msgid "Auto"
msgstr "Automatikus"

#: ../src/xfce_wallpaper_manager.c:385
msgid "Centered"
msgstr "Középre helyezett"

#: ../src/xfce_wallpaper_manager.c:388
msgid "Tiled"
msgstr "Mozaik"

#: ../src/xfce_wallpaper_manager.c:391
msgid "Stretched"
msgstr "Nyújtott"

#: ../src/xfce_wallpaper_manager.c:394
msgid "Scaled"
msgstr "Kifeszített"

#: ../src/xfce_wallpaper_manager.c:397
msgid "Zoomed"
msgstr "Nagyított"

#: ../ristretto.desktop.in.h:1
msgid "Ristretto Image Viewer"
msgstr "Ristretto képmegjelenítő"

#: ../ristretto.desktop.in.h:2
msgid "Look at your images easily"
msgstr "Képek egyszerű megjelenítése"

#: ../ristretto.appdata.xml.in.h:2
msgid ""
"The Ristretto Image Viewer is an application that can be used to view and "
"scroll through images, run a slideshow of images, open images with other "
"applications like an image-editor or configure an image as the desktop "
"wallpaper."
msgstr "A Ristretto képmegjelenítő képek megtekintésére, köztük görgetésére, más alkalmazásokkal történő megnyitására (pl. képszerkesztők), háttérként beállítására vagy diavetítés futtatására szolgáló alkalmazás."
