## What is it?

This is Ristretto, an image viewer for Xfce.

## Installation

The file [`INSTALL`](INSTALL) contains generic installation instructions.

## How to report bugs?

Bugs should be reported to the [Xfce bug tracking system](http://bugzilla.xfce.org)
against the product Ristretto. You will need to create an
account for yourself.

Please read the [`HACKING`](HACKING) file for information on where to send changes or
bugfixes for this package.
